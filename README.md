DataBase 为数据库

wpbc.data.csv 为测试数据

index.cc 为索引程序，主要用于建立数据库，

search.cc 为搜索程序，主要功能包括：利用Xapian的功能函数实现对输入关键字的搜索，并通过提供的计算权值的算法做排序；调用ReadFeature类、GenerateFeature类，以及用户自定义的功能部分调用机器学习模型，完成二轮排序

index、search 为编译后文件

model/LR.h 为机器学习模型，主要功能包括数据的初始化，预测模型部分，可直接调用（预测结果无参考价值）

model/readfeature.h 为接口文件，包括ReadFeature类、GenerateFeature类，分别用于实现对Xapian数据的提取，以及在调用机器学习模型前用于数据的基本处理，后续均可由不同需求做相应扩展

